<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Obiective;
use AppBundle\Form\ObiectiveType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Mapping as ORM;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $rep = $this->getDoctrine()->getRepository("AppBundle:Obiective");
        $allObjectives = $rep->findBy(
            array(
                "enabled" => true
            )
        );

        $objectivesArray = [];
        foreach ($allObjectives as $loc) {

            $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
            $path = $helper->asset($loc, 'imageFile');

            $categorie = $loc->getCategorie();

            $newLocation = [
                $loc->getName(),
                $categorie->getNume(),
                $loc->getAdresa(),
                $loc->getLat(),
                $loc->getLon(),
                "property-detail.html",
                $path,
                "assets/img/property-types/empty.png"
            ];

            array_push($objectivesArray, $newLocation);

        }

        $data = [
            "objectives" => $objectivesArray
        ];

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', $data);
    }

    /**
     * @Route("/obiective", name="turistice")
     */
    public function turisticeAction(Request $request)
    {

        $obiective = [];
        if (!empty($_GET)) {
            var_dump($_GET['categorie']);
            $obiective = $this->getDataByCategory($_GET['categorie'], $_GET['sector']);
        } else {
            $repository = $this->getDoctrine()->getRepository("AppBundle:Obiective");
            $obiective = $repository->findAll();
        }


        $data = [
            "titlu" => "Obiective turistice",
            "inreg" => "obiectivul turistic",
            "obiective" => $obiective
        ];

        return $this->render('default/baseList.html.twig', $data);
    }

    /**
     * @Route("/comerciale", name="comerciale")
     */
    public function comercialeAction(Request $request)
    {

        $data = [
            "titlu" => "Obiective comerciale",
            "inreg" => "obiectivul comercial"
        ];

        return $this->render('default/baseList.html.twig', $data);

    }

    /**
 * @Route("/restaurante", name="restaurante")
 */
    public function restauranteAction(Request $request)
    {
        $data = [
            "titlu" => "Restaurante",
            "inreg" => "restaurantul"
        ];

        return $this->render('default/baseList.html.twig', $data);
    }

    /**
     * @Route("/entertainment", name="entertainment")
     */
    public function entertainmentAction(Request $request)
    {
        $data = [
            "titlu" => "Entertainment",
            "inreg" => "obiectivul"
        ];

        return $this->render('default/baseList.html.twig', $data);
    }

    /**
     * @Route("/adauga", name="adauga")
     */
    public function adaugaAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
    * @Route("/despre-noi", name="despre-noi")
    */
    public function despreAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/my-profile", name="my-profile")
     */
    public function myProfile(Request $request)
    {
        return $this->render('default/profile_details.html.twig');
    }

    /**
 * @Route("/contact", name="contact")
 */
    public function contactAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/contact.html.twig');
    }

    /**
     * @Route("/success", name="success")
     */
    public function successAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/message.html.twig');
    }

    /**
     * @Route("/view-{id}", name="viewObiectiv")
     */
    public function viewAction(Request $request, Obiective $id)
    {

        $em = $this->getDoctrine()->getManager();

        $id->setViews($id->getViews() + 1);
        $em->flush();

        $data = [
            "obiectiv" => $id
        ];

        return $this->render('default/objective.html.twig', $data);
    }

    /**
     * @Route("/profile-categories", name="viewUserCategories")
     */
    public function viewUserCategoriesAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository("AppBundle:Obiective");

//        $query = $repository->createQueryBuilder('p')
//            ->join('p.categorie', 'c')
//            ->where('c.nume = :category')
//            ->setParameter('category', $category)
//            ->getQuery();

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $data = [
          "objectives" => $user//$query->getResult()
        ];

        return $this->render('default/profile_cat.html.twig', $data);
    }

    /**
     * @Route("/obiective/delete/{id}", name="deleteObiectiv")
     */
    public function deleteObiectiv(Obiective $item)
    {

        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('AppBundle:Obiective')->find($item);

        $em->remove($item);
        $em->flush();

        return $this->redirectToRoute('viewUserCategories');

    }

    /**
     * @Route("/create-account", name="createAccount")
     */
    public function createAccountAction(Request $request)
    {
        $form = $this->createForm(ObiectiveType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!

            $em = $this->getDoctrine()->getManager();


            $geocode = $this->geocode($task->getAdresa());
            if ($geocode) {
                $task->setLon($geocode[1]);
                $task->setLat($geocode[0]);
            } else {
                $task->setLon(0.0);
                $task->setLat(0.0);
            }


            $task->setFromUser(true);
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('success');
        } else {

        }

        return $this->render('default/account.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/adauga-obiectiv", name="adauga")
     */
    public function adaugaObiectiv(Request $request)
    {
        $form = $this->createForm(ObiectiveType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!

            $em = $this->getDoctrine()->getManager();


            $geocode = $this->geocode($task->getAdresa());
            if ($geocode) {
                $task->setLon($geocode[1]);
                $task->setLat($geocode[0]);
            } else {
                $task->setLon(0.0);
                $task->setLat(0.0);
            }

            $user = $this->container->get('security.token_storage')->getToken()->getUser();

            $task->setUsers($user);

            $task->setSectorName($task->getSector()->getName());

            $task->setFromUser(true);
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('success');
        } else {

        }

        return $this->render('default/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editeaza-obiectiv-{id}", name="editeazaObiectiv")
     */
    public function editeazaObiectiv(Request $request, Obiective $id)
    {
        $form = $this->createForm(ObiectiveType::class, $id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!

            $em = $this->getDoctrine()->getManager();


            $geocode = $this->geocode($task->getAdresa());
            if ($geocode) {
                $task->setLon($geocode[1]);
                $task->setLat($geocode[0]);
            } else {
                $task->setLon(0.0);
                $task->setLat(0.0);
            }

            $user = $this->container->get('security.token_storage')->getToken()->getUser();

            $task->setUsers($user);

            $task->setFromUser(true);
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('success');
        } else {

        }

        return $this->render('default/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    private function getDataByCategory($category, $sector)
    {

        $repository = $this->getDoctrine()->getRepository("AppBundle:Obiective");

        $query = $repository->createQueryBuilder('p')
            ->join('p.categorie', 'c')
            ->where('c.nume = :category')
            ->andWhere('p.sectorName = :sector')
            ->setParameter('category', $category)
            ->setParameter('sector', $sector)
            ->getQuery();

        return $query->getResult();

    }

    function geocode($address){

        // url encode the address
        $address = urlencode($address);

        // google map geocode api url
        $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

        // get the json response
        $resp_json = file_get_contents($url);

        // decode the json
        $resp = json_decode($resp_json, true);

        // response status will be 'OK', if able to geocode given address
        if($resp['status']=='OK'){

            // get the important data
            $lati = $resp['results'][0]['geometry']['location']['lat'];
            $longi = $resp['results'][0]['geometry']['location']['lng'];
            $formatted_address = $resp['results'][0]['formatted_address'];

            // verify if data is complete
            if($lati && $longi && $formatted_address){

                // put the data in the array
                $data_arr = array();

                array_push(
                    $data_arr,
                    $lati,
                    $longi,
                    $formatted_address
                );

                return $data_arr;

            }else{
                return false;
            }

        }else{
            return false;
        }
    }
}
