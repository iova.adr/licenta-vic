<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * housing
 *
 * @ORM\Table(name="entity_housing")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\housingRepository")
 */
class Housing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="orar", type="object")
     */
    private $orar;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return housing
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return housing
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set orar
     *
     * @param \stdClass $orar
     *
     * @return housing
     */
    public function setOrar($orar)
    {
        $this->orar = $orar;

        return $this;
    }

    /**
     * Get orar
     *
     * @return \stdClass
     */
    public function getOrar()
    {
        return $this->orar;
    }
}
