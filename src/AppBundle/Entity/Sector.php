<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Sector
 *
 * @ORM\Table(name="sector")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SectorRepository")
 */
class Sector
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Obiective", mappedBy="sector")
     */
    private $objectives;

    public function __construct()
    {
        $this->objectives = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sector
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add objective
     *
     * @param \AppBundle\Entity\Obiective $objective
     *
     * @return Sector
     */
    public function addObjective(\AppBundle\Entity\Obiective $objective)
    {
        $this->objectives[] = $objective;

        return $this;
    }

    /**
     * Remove objective
     *
     * @param \AppBundle\Entity\Obiective $objective
     */
    public function removeObjective(\AppBundle\Entity\Obiective $objective)
    {
        $this->objectives->removeElement($objective);
    }

    /**
     * Get objectives
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjectives()
    {
        return $this->objectives;
    }

    /**
     * String representation of this object
     * @return string
     */
    public function __toString()
    {
        try {
            return (string) $this->name;
        } catch (Exception $exception) {
            return '';
        }
    }
}
