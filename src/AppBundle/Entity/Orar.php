<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * Orar
 *
 * @ORM\Table(name="entity_orar")
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\OrarRepository")
 */
class Orar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inter", type="string", length=255)
     */
    private $inter;

    /**
     * @ORM\OneToMany(targetEntity="Obiective", mappedBy="orar")
     */
    private $objectives;

    public function __construct()
    {
        $this->objectives = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inter
     *
     * @param string $inter
     *
     * @return Orar
     */
    public function setInter($inter)
    {
        $this->inter = $inter;

        return $this;
    }

    /**
     * Get inter
     *
     * @return string
     */
    public function getInter()
    {
        return $this->inter;
    }

    /**
     * Add objective
     *
     * @param \AppBundle\Entity\Obiective $objective
     *
     * @return Orar
     */
    public function addObjective(\AppBundle\Entity\Obiective $objective)
    {
        $this->objectives[] = $objective;

        return $this;
    }

    /**
     * Remove objective
     *
     * @param \AppBundle\Entity\Obiective $objective
     */
    public function removeObjective(\AppBundle\Entity\Obiective $objective)
    {
        $this->objectives->removeElement($objective);
    }

    /**
     * Get objectives
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjectives()
    {
        return $this->objectives;
    }

    /**
     * String representation of this object
     * @return string
     */
    public function __toString()
    {
        try {
            return (string) $this->inter;
        } catch (Exception $exception) {
            return '';
        }
    }
}
