<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clients
 *
 * @ORM\Table(name="entity_clients")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\ClientsRepository")
 */
class Clients
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="cnp", type="integer")
     */
    private $cnp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_nasterii", type="date")
     */
    private $dataNasterii;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Clients
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cnp
     *
     * @param integer $cnp
     *
     * @return Clients
     */
    public function setCnp($cnp)
    {
        $this->cnp = $cnp;

        return $this;
    }

    /**
     * Get cnp
     *
     * @return int
     */
    public function getCnp()
    {
        return $this->cnp;
    }

    /**
     * Set dataNasterii
     *
     * @param \DateTime $dataNasterii
     *
     * @return Clients
     */
    public function setDataNasterii($dataNasterii)
    {
        $this->dataNasterii = $dataNasterii;

        return $this;
    }

    /**
     * Get dataNasterii
     *
     * @return \DateTime
     */
    public function getDataNasterii()
    {
        return $this->dataNasterii;
    }
}
