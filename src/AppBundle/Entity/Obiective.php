<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Obiective
 *
 * @ORM\Table(name="entity_obiective")
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\ObiectiveRepository")
 */
class Obiective
{
    /**
     * It only stores the name of the image associated with the product.
     *
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $image;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the product.
     *
     * @Vich\UploadableField(mapping="objective_images", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * Indicate if the product is enabled (available in store).
     *
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $enabled = false;

    /**
     *
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $fromUser = false;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="views", type="integer")
     */
    private $views = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="lon", type="float", precision = 7, )
     */
    private $lon;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision = 7)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sectorName", type="string", length=255)
     */
    private $sectorName = "None";

    /**
     * @var string
     *
     * @ORM\Column(name="adresa", type="string", length=255)
     */
    private $adresa;

    /**
     * @ORM\ManyToOne(targetEntity="Orar", inversedBy="objectives")
     * @ORM\JoinColumn(name="orar_id", referencedColumnName="id")
     */
    private $orar;

    /**
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="objectives")
     * @ORM\JoinColumn(name="categorie_id", referencedColumnName="id")
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity="Sector", inversedBy="objectives")
     * @ORM\JoinColumn(name="sector_id", referencedColumnName="id")
     */
    private $sector;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="objectives")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $users;

    /**
     * @var string
     *
     * @ORM\Column(name="descriere", type="string", length=2500)
     */
    private $descriere;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Obiective
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set adresa
     *
     * @param string $adresa
     *
     * @return Obiective
     */
    public function setAdresa($adresa)
    {
        $this->adresa = $adresa;

        return $this;
    }

    /**
     * Get adresa
     *
     * @return string
     */
    public function getAdresa()
    {
        return $this->adresa;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Obiective
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set lon
     *
     * @param float $lon
     *
     * @return Obiective
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return Obiective
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set orar
     *
     * @param \AppBundle\Entity\Orar $orar
     *
     * @return Obiective
     */
    public function setOrar(\AppBundle\Entity\Orar $orar = null)
    {
        $this->orar = $orar;

        return $this;
    }

    /**
     * Get orar
     *
     * @return \AppBundle\Entity\Orar
     */
    public function getOrar()
    {
        return $this->orar;
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * Set descriere
     *
     * @param string $descriere
     *
     * @return Obiective
     */
    public function setDescriere($descriere)
    {
        $this->descriere = $descriere;

        return $this;
    }

    /**
     * Get descriere
     *
     * @return string
     */
    public function getDescriere()
    {
        return $this->descriere;
    }

    /**
     * @param File $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set if the product is enable.
     *
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Is the product enabled?
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Alias of getEnabled.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->getEnabled();
    }


    /**
     *
     * @param bool $fromUser
     */
    public function setFromUser($fromUser)
    {
        $this->fromUser = $fromUser;
    }

    /**
     *
     * @return bool
     */
    public function getFromUser()
    {
        return $this->fromUser;
    }

    /**
     *
     * @return bool
     */
    public function isFromUser()
    {
        return $this->getFromUser();
    }




    /**
     * Set users
     *
     * @param \AppBundle\Entity\User $users
     *
     * @return Obiective
     */
    public function setUsers(\AppBundle\Entity\User $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set views
     *
     * @param integer $views
     *
     * @return Obiective
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set sector
     *
     * @param \AppBundle\Entity\Sector $sector
     *
     * @return Obiective
     */
    public function setSector(\AppBundle\Entity\Sector $sector = null)
    {
        $this->sector = $sector;

        return $this;
    }

    /**
     * Get sector
     *
     * @return \AppBundle\Entity\Sector
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * Set sectorName
     *
     * @param string $sectorName
     *
     * @return Obiective
     */
    public function setSectorName($sectorName)
    {
        $this->sectorName = $sectorName;

        return $this;
    }

    /**
     * Get sectorName
     *
     * @return string
     */
    public function getSectorName()
    {
        return $this->sectorName;
    }
}
