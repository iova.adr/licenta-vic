<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Obiective", mappedBy="users")
     */
    private $objectives;

    public function __construct()
    {
        $this->objectives = new ArrayCollection();
    }

    /**
     * Add objective
     *
     * @param \AppBundle\Entity\Obiective $objective
     *
     * @return User
     */
    public function addObjective(\AppBundle\Entity\Obiective $objective)
    {
        $this->objectives[] = $objective;

        return $this;
    }

    /**
     * Remove objective
     *
     * @param \AppBundle\Entity\Obiective $objective
     */
    public function removeObjective(\AppBundle\Entity\Obiective $objective)
    {
        $this->objectives->removeElement($objective);
    }

    /**
     * Get objectives
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjectives()
    {
        return $this->objectives;
    }
}
