<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incasari
 *
 * @ORM\Table(name="entity_incasari")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\IncasariRepository")
 */
class Incasari
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="valoare", type="integer")
     */
    private $valoare;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="date")
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="time")
     */
    private $time;

    /**
     * @var float
     *
     * @ORM\Column(name="tva", type="float")
     */
    private $tva;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valoare
     *
     * @param integer $valoare
     *
     * @return Incasari
     */
    public function setValoare($valoare)
    {
        $this->valoare = $valoare;

        return $this;
    }

    /**
     * Get valoare
     *
     * @return int
     */
    public function getValoare()
    {
        return $this->valoare;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Incasari
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Incasari
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set tva
     *
     * @param float $tva
     *
     * @return Incasari
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva
     *
     * @return float
     */
    public function getTva()
    {
        return $this->tva;
    }
}
