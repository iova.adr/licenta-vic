<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bilete
 *
 * @ORM\Table(name="entity_bilete")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\BileteRepository")
 */
class Bilete
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="pret", type="integer")
     */
    private $pret;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255)
     */
    private $categorie;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pret
     *
     * @param integer $pret
     *
     * @return Bilete
     */
    public function setPret($pret)
    {
        $this->pret = $pret;

        return $this;
    }

    /**
     * Get pret
     *
     * @return int
     */
    public function getPret()
    {
        return $this->pret;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Bilete
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
}
